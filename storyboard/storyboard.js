class Vec {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  
  clone() {
    return new Vec(this.x, this.y);
  }
  
  svg() {
    return this.x + ',' + this.y;
  }
  
  add(other) {
    this.x += other.x;
    this.y += other.y;
    return this;
  }
  
  plus(other) {
    return new Vec(this.x + other.x, this.y + other.y);
  }
  
  subtract(other) {
    this.x -= other.x;
    this.y -= other.y;
    return this;
  }
  
  minus(other) {
    return new Vec(this.x - other.x, this.y - other.y);
  }
  
  scale(s) {
    this.x *= s;
    this.y *= s;
    return this;
  }
  
  times(s) {
    return new Vec(this.x * s, this.y * s);
  }
  
  dot(other) {
    return this.x * other.x + this.y * other.y;
  }
  
  lengthSq() {
    return this.dot(this);
  }
  
  length() {
    return Math.sqrt(this.lengthSq());
  }
  
  distToSq(other) {
    return this.minus(other).lengthSq();
  }
  
  distTo(other) {
    return this.minus(other).length();
  }
  
  norm() {
    return this.times(1 / this.length());
  }
  
  angleTo(other) {
    return Math.acos(this.norm().dot(other.norm()));
  }
  
  rotate(angle) {
    let c = Math.cos(angle);
    let s = Math.sin(angle);
    return new Vec(
      c * this.x - s * this.y,
      s * this.x + c * this.y
    )
  }
}

addEventListener('load', init);

let nav = {
  scroller: null,
  current: 'login',
  windowSize: null,
  screenSize: null,
  
  free: false,
  position: null,
  zoom: 1,
  moving: false,
  lastMouse: null,
  
  minZoom: 3, // exponential
}

// Touch this part
let pages = {
  login: {
    x: 0, y: 0,
    to: ['main']
  },
  main: {
    x: 2, y: 2,
    to: ['login', 'media', 'players', 'layouts']
  },
  
  media: {
    x: 2, y: 0.8,
    to: ['main', 'mediaUpload', 'mediaInfo']
  },
  
  mediaUpload: {
    x: 1, y: 0,
    to: ['main', 'media', 'mediaInfo']
  },
  
  mediaInfo: {
    x: 3, y: 0,
    to: ['main']
  },

  players: {
    x: 0, y: 1,
    to: ['main', 'playerInfo']
  },

  playerInfo: {
    x: 0, y: 2,
    to: ['main']
  },

  layouts: {
    x: 4, y: 2,
    to: ['main', 'layoutEditor']
  },

  layoutEditor: {
    x: 4, y: 4,
    to: ['main']
  }
}
// But not the rest!

function init() {
  nav.scroller = document.getElementById('scroller');
  nav.windowSize = new Vec(innerWidth, innerHeight);
  nav.screenSize = new Vec(1024, 768);
  nav.wrap = document.getElementById('wrap');
  
  seekPage(location.hash ? location.hash.slice(2) : 'main');
  
  document.body.addEventListener('keyup', keyUp);
  document.body.addEventListener('mousedown', mouseDown);
  document.body.addEventListener('mouseup', mouseUp);
  document.body.addEventListener('mousemove', mouseMove);
  document.body.addEventListener('wheel', wheel, true);
  
  // Draw all the arrows
  let centerOffset = nav.screenSize.times(0.5);
  for (let [name, data] of Object.entries(pages)) {
    let fromCenter = nav.windowSize.clone();
    fromCenter.x *= data.x;
    fromCenter.y *= data.y;
    fromCenter.add(centerOffset);

    for (let to of data.to) {
      let other = pages[to];
      let toCenter = nav.windowSize.clone();
      toCenter.x *= other.x;
      toCenter.y *= other.y;
      toCenter.add(centerOffset);
      
      //if (name === 'main' && to === 'media') debugger;
      
      // Scale down a bit to exclude a circle of radius at least 640
      let rad = 600;
      let delta = toCenter.minus(fromCenter);
      
      // Make smaller if towards the top
      let angle = delta.angleTo(new Vec(0, 1));
      angle = Math.min(angle, Math.PI - angle);
      if (angle < Math.PI / 5) rad -= 150;

      let offset = delta.norm().scale(rad);
      let fromCenterOffset = fromCenter.plus(offset);
      toCenter.subtract(offset);
      
      // The other points on the arrow
      let arrowBack = toCenter.minus(fromCenterOffset).norm().scale(100).rotate(Math.PI);
      let leftArrow = toCenter.plus(arrowBack.rotate(Math.PI / 6));
      let rightArrow = toCenter.plus(arrowBack.rotate(-Math.PI / 6));
      
      // Add to svg
      let svgNS = "http://www.w3.org/2000/svg";  
      let arrow = document.createElementNS(svgNS, 'polyline');
      arrow.setAttribute('points',
        [fromCenterOffset, toCenter, leftArrow, toCenter, rightArrow]
        .map(e => e.svg()).join(' ')
      )
      
      console.log(arrow);
      
      document.getElementById('arrows').appendChild(arrow);
      
      console.log('From', fromCenter, 'to', toCenter);
    }
  }
}

function keyUp(evt) {
  if (evt.key === '`') {
    toggleFree();
    
    if (!nav.free)
      seekPage(nav.current, true)
  }
}

function mouseDown(evt) {
  if (evt.target !== document.body) return;
  if (!nav.free) return;
  
  nav.moving = true;
  nav.lastMouse = new Vec(evt.pageX, evt.pageY);
  nav.scroller.classList.add('moving');
}

function mouseUp(evt) {
  //if (evt.target !== document.body) return;
  if (!nav.free) return;
  
  nav.moving = false;
  nav.scroller.classList.remove('moving');
}

function mouseMove(evt) {
  if (!(nav.free && nav.moving)) return;
  
  let dbg = document.getElementById('dbgPtr').style;
  
  let mouse = new Vec(evt.pageX, evt.pageY);
  let delta = mouse.minus(nav.lastMouse).times(1 / nav.zoom);
  nav.lastMouse = mouse;
  
  let next = nav.position;
  next.add(delta);
  
  nav.scroller.style.transform = 'translate(' + next.x + 'px,' + next.y + 'px)';
}

function wheel(evt) {
  //if (evt.target !== document.body) return;
  if (!nav.free) return;
  //if (nav.moving) return; // Eh...
  evt.stopPropagation();
  evt.preventDefault();
  
  let s = Math.pow(2, 1 / 4);
  
  // Zoom in
  if (nav.zoom < 1 && evt.deltaY < 0)
    nav.zoom *= s;
  else if (nav.zoom > 2 ** -nav.minZoom && evt.deltaY > 0)
    nav.zoom /= s;
  
  
  
  /*nav.scroller.style.transform =
  'translate(' + nav.position.x + 'px,' + nav.position.y + 'px) ' +
  'scale(' + nav.zoom + ')';*/
  
  nav.wrap.style.transform = 'scale(' + nav.zoom + ')';
}

function toggleFree(set) {
  if (typeof set !== 'undefined')
    nav.free = set;
  else
    nav.free = !nav.free;
  
  document.body.style.backgroundColor = nav.free ? '#182' : '#124';
}

function seekPage(name, preserveFree = false) {
  let margin = nav.windowSize.minus(nav.screenSize).scale(0.5);
  
  let next = nav.windowSize.clone();
  next.x *= pages[name].x;
  next.y *= pages[name].y;
  next.scale(-1);
  next.add(margin);
  
  nav.position = next;
  
  nav.scroller.style.transform = 'translate(' + next.x + 'px,' + next.y + 'px)';
  
  // Zoom standard
  nav.zoom = 1;
  nav.wrap.style.transform = 'scale(' + nav.zoom + ')';
  
  nav.current = name;
  location.hash = '!' + name;
  
  if (!preserveFree)
    toggleFree(false);
}